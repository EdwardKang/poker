require 'rspec'
require 'poker'

describe Card do
  let(:card) { Card.new("A", "clubs") }
  describe "valid_card?" do
    it "returns the value of the card" do
      expect(card.valid_card?("A", "clubs")).to eq(true)
    end
  end


  describe "display" do
    it "displays" do
      expect(card.display).to eq("AC")
    end
  end

  describe "shuffle_deck" do
    it "returns a shuffled deck" do
      expect(card.create_deck.length).to eq(52)

    end
  end
end

describe Hand do
  let(:hand) { Hand.new }

  describe "include_pair?" do
    it "returns true if it's a pair" do
      expect(hand.include_pair?(["AC", "AS", "2S", "8S", "10H"])).to eq(true)
    end
  end

  describe "include_two_pair?" do
    it "return true if it's a two pair" do
      expect(hand.include_two_pair?(["AC", "AS", "8S", "8S", "10H"])).to eq(true)
    end
  end

  describe "three_of_a_kind?" do
    it "returns true if it's three of a kind" do
      expect(hand.three_of_a_kind?(["AC", "AS", "AH", "8S", "10H"])).to eq(true)
    end
  end

  describe "straight?" do
    it "returns true if it's straight" do
      expect(hand.straight?(["2C", "3S", "4H", "5S", "6H"])).to eq(true)
    end
  end

  describe "flush?" do
    it "returns true if it flushes" do
      expect(hand.flush?(["2S", "3S", "4S", "5S", "6S"])).to eq(true)
    end
  end

  describe "full_house?" do
    it "returns true if full house" do
      expect(hand.full_house?(["2S", "2S", "2S", "3S", "3S"])).to eq(true)
    end
  end

  describe "four_of_a_kind?" do
    it "returns true if four of a kind " do
      expect(hand.four_of_a_kind?(["2S", "2S", "2S", "2S", "3S"])).to eq(true)
    end
  end

  describe "straight_flush?" do
    it "returns true if straight and flush " do
      expect(hand.straight_flush?(["2S", "3S", "4S", "5S", "6S"])).to eq(true)
    end
  end
end
