class Card
  attr_accessor :value, :suit, :value_arr, :suit_arr

  def initialize(value, suit)
    @value = value
    @suit = suit
    @value_arr = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J',
      'Q', 'K', 'A']
    @suit_arr = ['hearts','clubs','spades','diamonds']
  end

  def shuffle_deck
    combinations = []
    shuffle_values = self.value_arr
    shuffle_suits  = self.suit_arr

    shuffle_values.each do |value|
      shuffle_suits.each do |suit|
        combinations << [value, suit] if valid_card?(value, suit)
      end
    end

    combinations
  end

  def create_deck
    deck = []
    shuffle_deck.each do |combination|
      deck << Card.new(combination[0], combination[1])
    end

    new_shuffle(deck)
  end

  def valid_card?(value, suit)
    value_arr.include?(value) && suit_arr.include?(suit)
  end

  def display
     self.value + self.suit[0].upcase
  end

  def new_shuffle(deck)
    deck = deck.shuffle
  end
end

class Hand

  attr_accessor :random_card, :deck
  def initialize
    @random_card = Card.new("A", "clubs")
    @deck = random_card.create_deck
  end

  def hand(deck)
    new_hand = deck.shift(5)
    new_hand.map { |card| card.display }
  end

  def include_pair?(new_hand)
    count_card_values(new_hand).each do |key, value|
      return value >= 2
    end
  end

  def count_card_values?(new_hand)
    count_hash = {}
    new_hand.each do |card|
      if count_hash.include?(card[0])
        count_hash[card[0]] += 1
      else
        count_hash[card[0]] = 1
      end
    end
    count_hash
  end

  def include_two_pair?(new_hand)
    pair_count = 0
    count_card_values(new_hand).each do |key, value|
      pair_count += 1 if value >= 2
    end
    pair_count >= 2
  end

  def three_of_a_kind?(new_hand)
    count_card_values(new_hand).each do |key, value|
      return value >= 3
    end
  end

  def straight?(new_hand)
    low_to_high = ["A", "2", '3', '4', '5', '6', '7', '8', '9',
                   '1', 'J', 'Q', 'K', 'A']
    sort_hand = new_hand.map { |card| card[0] }
    sort_hands = sort_hand.sort
    10.times do |start|
      return true if low_to_high[start, 5].sort == sort_hands
    end
  end

  def flush?(new_hand)
    suit_count = 0
    counting_suits = [new_hand[0][-1]]
    new_hand.each do |card|
      suit_count += 1 if counting_suits.include?(card[-1])
    end

    suit_count == 5
  end

  def full_house?(new_hand)
    three_of_a_kind(new_hand) && include_pair?(new_hand)
  end

  def four_of_a_kind?(new_hand)
    count_card_values(new_hand).each do |key, value|
      return value >= 4
    end
  end

  def straight_flush?(new_hand)
    straight(new_hand) && flush(new_hand)
  end


  def hand_values(new_hand)
    hand_value_hash = { pair:  0,
                        two_pair: 1,
                        three_of_a_kind: 2,
                        straight: 3,
                        flush: 4,
                        full_house: 5,
                        four_of_a_kind: 6,
                        straight_flush: 7
    }
    new_hand.each do |card|
      if count_hash.include?(card[0])
        count_hash[card[0]] += 1
      else
        count_hash[card[0]] = 1
      end
    end
    count_hash
  end
end






