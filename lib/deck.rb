require 'poker'

class Deck
  attr_accessor :value_arr, :suit_arr, :deck

  def initialize
    @deck = create_deck
    @value_arr = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J',
      'Q', 'K', 'A']
    @suit_arr = ['hearts','clubs','spades','diamonds']
  end
end
